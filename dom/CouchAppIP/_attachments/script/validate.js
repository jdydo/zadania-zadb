function validate_connection() {
    var number = document.getElementById("id_connection_textbox").value;
    if (number == null || isNaN(number) || number < 1) {
        alert("Wprowadzone dane są nieprawidłowe!")
    } else {
        show_by_connection_list()
    }
}

function validate_download() {
    var number = document.getElementById("id_download_textbox").value;
    if (number == null || isNaN(number) || number < 1) {
        alert("Wprowadzone dane są nieprawidłowe!")
    } else {
        show_by_download_list()
    }
}