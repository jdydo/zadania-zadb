// Tworze zmienna z uchwytem do bazy
db = $.couch.db("p1");

function show_by_connection_list() {
    // Czyszcze tbody
    $("#id_connection_list").empty();

    // Uruchamiam map-reduce
    db.view("CouchAppIP/byconnection",
    {
        success: function (data) {
            // Sortowanie od najwiekszego
            data.rows.sort(function(a, b) { return b.value - a.value });

            var number = document.getElementById("id_connection_textbox").value;
            var iterator = 1;
            var table = '<div class="panel panel-default">' +
                            '<table class="table">' +
                                '<thead>' +
                                  '<tr>' +
                                    '<th>#</th>' +
                                    '<th>Adres IP</th>' +
                                    '<th>Liczba połączeń</th>' +
                                 '</tr>' +
                                '</thead>' +
                                '<tbody id="id_connection_tbody">';

            // Wypisywanie wyniku map-reduce ubranego w odpowiednie tagi html
            for (i in data.rows.slice(0, number)) {
                table += '<tr>' +
                             '<td>'+ iterator +'</td>' +
                             '<td>' + data.rows[i].key + '</td>' +
                             '<td>' + data.rows[i].value + '</td>' +
                         '</tr>';
                iterator++;
            }

            table += '</tbody></table></div>';

            $("#id_connection_list").append(table);
        },

        error: function() {
            alert('Coś poszło nie tak! Spróbuj ponownie później.')
        },

        // Wlaczam redukowanie i grupowanie
        reduce: true,
        group: true
    });
}