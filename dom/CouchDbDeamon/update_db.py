import couchdb
from parser import parse
from time import sleep
import logging


def create_logger():
    logging.basicConfig(
        filename='DAEMON.log',
        level=logging.DEBUG,
        format='%(asctime)s - %(levelname)s - %(message)s'
    )


def reset(db):
    i = 0
    for id in db:
        if (not id == 'config1') and (not id == '_design/CouchAppIP'):
            del db[id]
            i += 1
    logging.info('Usunieto %s rekordow' % str(i))


def update_db():
    couch = couchdb.Server('http://194.29.175.241:5984/')
    #couch = couchdb.Server('http://localhost:5984/')
    db = couch['p1']

    create_logger()
    logging.info('Daemon dziala')
    reset(db)

    while True:
        try:
            result = parse('/var/log/nginx/access.log')
            records_number = len(db) - 2

            if len(result) > records_number:

                new_result = result[records_number:]
                i = 0

                for item in new_result:
                    try:
                        db.save(item)
                        i += 1
                    except Exception, e:
                        logging.exception('Wystapil blad!')

                logging.info('Dodano %s z %s rekordow *' % (str(i), str(len(new_result))))

            elif len(result) < records_number:
                reset(db)

                new_result = result
                i = 0
                for item in new_result:
                    try:
                        db.save(item)
                        i += 1
                    except Exception, e:
                        logging.exception('Wystapil blad!')

                logging.info('Dodano %s z %s rekordow **' % (str(i), str(len(new_result))))

            else:
                logging.info('Nie dodano nowych rekordow')

            try:
                update_time = db['config1']['update_time']
                logging.info('Wstrzymanie pracy na %ss' % str(update_time))
                sleep(update_time)
            except Exception, e:
                del db['config1']
                db['config1'] = {'update_time': 120}
                logging.exception('Wstrzymanie pracy po bledzie na 120s')
                sleep(120)

        except Exception, e:
            logging.exception('Wystapil blad!')
            logging.info('Wstrzymanie pracy po bledzie na 120s')
            sleep(120)