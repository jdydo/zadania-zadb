# -*- encoding: utf-8 -*-

import signal
import daemon
import lockfile
from update_db import update_db


context = daemon.DaemonContext(
    working_directory='/home/p1',
    pidfile=lockfile.FileLock('/home/p1/daemonp1.pid'),
)

context.signal_map = {
    signal.SIGTERM: 'terminate',
}

with context:
    update_db()