import sys
import apachelog


def parse(filename):
    format = r'ip x xx %{Date}t \"request\" http_code download \"address\" \"user-agent\"'
    parser = apachelog.parser(format)

    output = []
    with open(filename, 'r') as file:
        for line in file:
            try:
                output.append(parser.parse(line))
            except:
                sys.stderr.write("Problem z parsowaniem %s" % line)
    return  output