
Praca domowa #B
===============

Stworzyć aplikację webową serwowaną z CouchDB (*CouchApp*), która na bieżąco wyświetla
na stronie dane o połączeniach z serwerem Nginx (plik ``/var/log/nginx/access.log``)
pokazujące:

* N adresów IP użytkowników, którzy najczęściej się łączyły (wraz z liczbą połączeń)
* N adresów IP użytkowników, którzy pobrali najwięcej danych (wraz z liczbą pobranych danych)

Parametr N powinien być konfigurowany przez użytkownika.

ps. W celu poprawnego wykonania zadania i zasilania danych na bieżąco, na serwerze powinien
chodzić demon, który w określonym interwale czasu kofigurowalnym w bazie CouchDB będzie odświeżał
dane w bazie na podstawie wpisów w ``/var/log/nginx/access.log``.
