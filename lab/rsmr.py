# -*- coding: utf-8 -*-
# Copyright 2014 Mikołaj Olszewski
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from multiprocessing import Pool
from collections import defaultdict
from itertools import chain, repeat


######################################
#         Helper Functions           #
######################################

def map_wrapper(args):
    data = args[0]
    mapfn = args[1]
    return [x for x in mapfn(data)]


def reduce_wrapper(args):
    data = args[0]
    reducefn = args[1]
    return [x for x in reducefn(data[0], data[1])]


def reduce_identity(key, values):
    yield key, values


######################################
#            Main Class              #
######################################

class MapReduce(object):
    """Multiprocess single machine MapReduce helper.
    """

    def __init__(self, mapfn, reducefn=reduce_identity, num_workers=None):
        """MapReduce constructor. Creates MapReduce factory class.

        Args:
            mapfn (function): The map function (item) -> None
                It uses statement ``yield key, value`` for generating
                any number of mappings.

        Kwargs:
            reducefn (function): The reduce function (key, values) -> object
                It returns aggregated data for current key. Default value
                is an identity function which returns input params.

            num_workers (int): Number of processes to execute.
                In case of None the number of processes is equal
                to number of available cores all cpu.
        """
        self.mapfn = mapfn
        self.reducefn = reducefn
        self.pool = Pool(num_workers)

    def __partition(self, mapped_values):
        """Groups together all values with the same key.

        Returns:
            unsorted list of (key, values)
        """
        partitioned_data = defaultdict(list)
        for key, value in mapped_values:
            partitioned_data[key].append(value)
        return partitioned_data.items()

    def __call__(self, inputs, chunksize=1):
        """Processes input date with MapReduce processing model.

        Args:
            inputs (iterable): Iterable input data to process.

        Kwargs:
            chunksize (int): Size of input data passed one in time for one process.
        """
        map_results = self.pool.map(map_wrapper, zip(inputs, repeat(self.mapfn)), chunksize=chunksize)
        partitioned_data = self.__partition(chain(*map_results))
        reduced_values = self.pool.map(reduce_wrapper, zip(partitioned_data, repeat(self.reducefn)))
        return list(chain(*reduced_values))
