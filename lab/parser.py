import sys
import apachelog
import couchdb


def parse(filename):
    format = r'%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"'
    parser = apachelog.parser(format)

    with open(filename, 'r') as file:
        for line in file:
            try:
                row = parser.parse(line)
                row['%t'] = row['%t'][1:12]+' '+row['%t'][13:21]+' '+row['%t'][22:27]
                yield row
            except apachelog.ApacheLogParserError:
                sys.stderr.write("Problem z parsowaniem %s" % line)

couch = couchdb.Server('http://194.29.175.241:5984/')

db = couch['p1']

result = parse('access1.log')

for r in result:
    try:
        db.save(r)
        print 'ok'
    except Exception, e:
        print e