# -*- coding: utf-8 -*-

import json
import unittest

from rsmr import MapReduce


def map(item):
    yield int(item[1]), [item[0]] + item[2:]


def reduce(key, values):
    out = []
    for value in values[1:]:
        out.append([str(key)] + values[0] + value)
    return out


def read_data(filename):
    data = open(filename)
    return json.load(data)


def join_records():
    input_data = read_data('records.json')
    mapper = MapReduce(map, reduce)
    results = mapper(input_data)
    return results


class JoinTest(unittest.TestCase):

    def test_results(self):
        expected = read_data('join.json')
        current = join_records()
        self.assertListEqual(sorted(expected), sorted(current))


if __name__ == '__main__':
    unittest.main()
